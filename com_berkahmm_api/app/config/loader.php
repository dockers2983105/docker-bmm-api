<?php

use Phalcon\Loader;

$loader = new Loader();

/**
 * Register Namespaces
 */
$loader->registerNamespaces([
    'Com_berkahmm_api\Models' => APP_PATH . '/common/models/',
    'Com_berkahmm_api'        => APP_PATH . '/common/library/',
]);

/**
 * Register module classes
 */
$loader->registerClasses([
    'Com_berkahmm_api\Modules\Frontend\Module' => APP_PATH . '/modules/frontend/Module.php',
    'Com_berkahmm_api\Modules\Cli\Module'      => APP_PATH . '/modules/cli/Module.php'
]);

$loader->register();
